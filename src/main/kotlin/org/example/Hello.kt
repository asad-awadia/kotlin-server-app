package org.example

import com.fasterxml.jackson.module.kotlin.registerKotlinModule
import com.github.benmanes.caffeine.cache.Caffeine
import io.javalin.Javalin
import io.vertx.core.json.jackson.DatabindCodec
import kong.unirest.Unirest
import org.jdbi.v3.core.Jdbi
import org.json.JSONObject
import org.ktorm.database.Database
import org.ktorm.dsl.from
import org.ktorm.dsl.insertAndGenerateKey
import org.ktorm.dsl.select
import org.ktorm.schema.Table
import org.ktorm.schema.int
import org.ktorm.schema.varchar
import java.util.concurrent.TimeUnit

fun main() {
  DatabindCodec.mapper().registerKotlinModule()
  println("Hello, World")

  // jdbi to sqlite
  val jdbi = Jdbi.create("jdbc:sqlite:mydata.db")
  // create table using jdbi
  jdbi.useHandle<Exception> {
    val sql = """create table if not exists requests (id integer primary key, name text)"""
    it.execute(sql)
  }

  // ktorm to sqlite
  val database = Database.connect("jdbc:sqlite:mydata.db")
  // insert row
  database.insertAndGenerateKey(Requests) {
    set(it.name, "Blocks_listBlocks")
  }
  // loop over table
  for (row in database.from(Requests).select()) {
    println("${row[Requests.id]} ${row[Requests.name]}")
  }

  // caffeine cache
  val cache = Caffeine.newBuilder()
    .expireAfterAccess(30, TimeUnit.MINUTES)
    .maximumSize(100)
    .build<String, String>()

  // redis client
  // val redisClient = RedisClient.create("redis://localhost").connect().sync()

  // http server
  val app = Javalin.create()
  app.get("/") {
    it.json(JSONObject().put("foo", JSONObject(Unirest.get("https://reqres.in/api/user/1").asString().body)).toString())
  }

  // curl -X PUT localhost:7000/cache -d '{"key": "foo", "value": "bar"}'
  app.put("/cache") {
    val putCacheRequest = JSONObject(it.body())
    val key = putCacheRequest.optString("key", "defaultKey")
    val value = putCacheRequest.optString("value", "defaultValue")
    cache.put(key, value)
    it.result(putCacheRequest.toString())
  }

  app.get("/cache/{key}") {
    val value = cache.getIfPresent(it.pathParam("key")) ?: "not-found"
    it.result(value)
  }

  app.get("/cache") {
    it.result(JSONObject(cache.asMap()).toString())
  }

  app.start(7000)
}

object Requests : Table<Nothing>("requests") {
  val id = int("id").primaryKey()
  val name = varchar("name")
}
