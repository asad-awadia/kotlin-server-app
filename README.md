# kotlin-server-app

Kotlin boiler plate to build a backend app

Includes:

- JDBI for database manipulation
- Ktorm as ORM
- Javalin for http server
- Lettuce as a redis client
- Caffeine as an in memory cache
- Unirest as an http client
- Jib to create docker images [mvn compile jib:dockerBuild]
- Maven assembly plugin to create fat jar [mvn package]
- Codehaus to run the app directly from terminal [mvn clean compile exec:java]
